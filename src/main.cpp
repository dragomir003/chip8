#include <variant>
#include <fstream>
#include <string_view>

#include <logging.hpp>
#include <pipeline.hpp>

auto main(const int argc, const char* argv[]) -> int {
	if (argc != 2) {
		logging::error("Usage: chip8 <binary-file>");
		return 1;
	}

	std::ifstream stream(argv[1], std::ios_base::binary);

	if (stream.fail()) {
		logging::error("Failed to load the file");
		return 1;
	}

	stream.seekg(0, std::ios_base::seekdir::_S_end);
	const size_t len = stream.tellg();
	stream.seekg(0, std::ios_base::seekdir::_S_beg);

	char* file = new char[len + 1];
	stream.get(file, len);
	file[len] = 0;

	const auto result = pipeline::run(file);

	if (result) {
		logging::error(*result);
		delete[] file;
		return 1;
	}

	delete[] file;

	return 0;
}
