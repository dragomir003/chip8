#include <logging.hpp>

#include <memory>
#include <string_view>

#include <spdlog/sinks/stdout_color_sinks.h>

static std::shared_ptr<spdlog::logger> logger;

namespace logging {
	auto init() -> void {
		logger = spdlog::stderr_color_mt("logger");
		logger->set_level(spdlog::level::level_enum::trace);
	}

	auto info(const std::string_view msg) -> void {
		logger->info(msg);
	}
	auto warn(const std::string_view msg) -> void {
		logger->info(msg);
	}
	auto error(const std::string_view msg) -> void {
		logger->error(msg);
	}
}
