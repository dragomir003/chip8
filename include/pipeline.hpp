#pragma once

#include <optional>
#include <string>

namespace pipeline {
	auto run(const char* file) -> std::optional<std::string>;
}
