#pragma once

#include <string_view>

namespace logging {
	auto init() -> void;

	auto info(const std::string_view msg) -> void;
	auto warn(const std::string_view msg) -> void;
	auto error(const std::string_view msg) -> void;
}
